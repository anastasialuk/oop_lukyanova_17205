import counter.CsvDataWriter;
import counter.TextWordParser;
import counter.WordCounter;

import java.io.*;

public class Main {
    public static void main(String[] args){
        WordCounter wordCounter = new WordCounter();
        try {
            InputStream in;
            OutputStream out;
            if(args.length == 0) {
                in = System.in;
                out = System.out;
            }
            else if(args.length == 2){
                in = new FileInputStream(args[0]);
                out = new FileOutputStream(args[1]);
            }
            else {
                System.err.println("Wrong number of arguments!");
                return;
            }
            try(InputStreamReader reader = new InputStreamReader(in);
                OutputStreamWriter writer = new OutputStreamWriter(out)) {
                wordCounter.readFile(new TextWordParser(reader));
                wordCounter.writeFile(new CsvDataWriter(writer));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("END");
    }
}
