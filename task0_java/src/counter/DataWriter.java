package counter;

import java.io.IOException;

public interface DataWriter {
    void writeRow(String ... columns) throws IOException;
}
