package counter;

import java.io.*;

public class TextWordParser implements WordParser {
    private Reader in;
    public TextWordParser(Reader is) {
        in = is;
    }
    @Override
    public String getWord() throws IOException {
        StringBuilder sb = new StringBuilder();
        int character;
        while ((character = in.read()) > 0)
        {
            if (Character.isLetterOrDigit(character))
            {
                sb.append((char)character);
            }
            else {
                if(sb.length()==0){
                    continue;
                }
                break;
            }
        }
        return (sb.length()==0 && character < 0)? null: sb.toString();
    }

}
