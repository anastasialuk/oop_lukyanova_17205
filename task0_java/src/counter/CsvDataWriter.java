package counter;

import java.io.*;

public class CsvDataWriter implements DataWriter {
    private Writer out;
    public CsvDataWriter(Writer os) {
        out = os;
    }

    @Override
    public void writeRow(String ... columns) throws IOException {
        out.write(String.join(",",columns));
        out.write(System.lineSeparator());
    }
}
