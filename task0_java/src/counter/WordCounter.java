package counter;

import java.io.IOException;
import java.util.*;

public class WordCounter {
    private int allWordCounter = 0;
    private Map<String, Integer> wordMap = new HashMap<>();

    public void readFile(WordParser parser) throws IOException {
        String word;
        while ((word = parser.getWord()) != null) {
            wordMap.put(word, wordMap.getOrDefault(word, 0) + 1);
            allWordCounter++;
        }
    }

    public void writeFile(DataWriter writer) throws IOException {
        List<Map.Entry<String, Integer>> sortedWordList = new ArrayList<>(wordMap.entrySet());
        sortedWordList.sort(this::cmp);

        for (Map.Entry<String, Integer> item : sortedWordList) {
            writer.writeRow(
                    item.getKey(),
                    item.getValue().toString(),
                    Double.toString(100. * item.getValue() / allWordCounter)
            );
        }
    }

    private int cmp(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
        int result = o2.getValue().compareTo(o1.getValue());
        return result ==0 ? o1.getKey().compareTo(o2.getKey()) : result;
    }
}

