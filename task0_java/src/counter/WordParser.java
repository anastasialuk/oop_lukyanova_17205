package counter;

import java.io.IOException;

public interface WordParser {
    String getWord() throws IOException;

}
