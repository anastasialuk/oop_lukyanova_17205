package calculator;

import calculator.exceptions.CommandException;

public interface Command {
    void execute(Calculator.CalculatorContext context, String[] args) throws CommandException;

}
