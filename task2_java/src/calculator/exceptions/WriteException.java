package calculator.exceptions;

public class WriteException extends CommandException {
    public WriteException(){
        super("Unable to write data");
    }
}

