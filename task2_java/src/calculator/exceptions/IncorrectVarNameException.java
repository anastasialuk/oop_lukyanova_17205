package calculator.exceptions;

public class IncorrectVarNameException extends CommandException {
    public IncorrectVarNameException(){
        super("Incorrect variable name");
    }
}
