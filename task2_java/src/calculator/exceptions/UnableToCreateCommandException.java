package calculator.exceptions;

public class UnableToCreateCommandException extends CommandException {
    public UnableToCreateCommandException(){
        super("Unable to create command");
    }
    public UnableToCreateCommandException(Throwable ex){
        super("Unable to create command", ex);
    }
}
