package calculator.exceptions;

public class NotEnoughElementsInStackException extends CommandException {
    public NotEnoughElementsInStackException(){
        super("Not enough elements in stack");
    }
}
