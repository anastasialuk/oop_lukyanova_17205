package calculator.exceptions;

public class CommandNotFoundException extends CommandException {
    public CommandNotFoundException(){
        super("Command not found");
    }
}
