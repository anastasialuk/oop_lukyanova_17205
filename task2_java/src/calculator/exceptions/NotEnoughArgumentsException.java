package calculator.exceptions;

public class NotEnoughArgumentsException extends CommandException {
    public NotEnoughArgumentsException(){
        super("Not enough arguments");
    }
}
