package calculator.exceptions;

public class GetCommandNameException extends CommandException {
    public GetCommandNameException(){
        super("Unable to get command name");
    }
}
