package calculator.exceptions;

public class SqrtFromNegativeValueException extends CommandException {
    public SqrtFromNegativeValueException(){
        super("Unable to calculate sqrt from negative value");
    }
}
