package calculator.exceptions;

public class ConvertStrToDoubleException extends CommandException {
    public ConvertStrToDoubleException(){
        super("Unable to convert string to double");
    }

    public ConvertStrToDoubleException(Throwable ex) {
        super("Unable to convert string to double", ex);
    }
}
