package calculator.exceptions;

public class VariableNotFoundException extends CommandException {
    public VariableNotFoundException(){
        super("Variable not found");
    }
}
