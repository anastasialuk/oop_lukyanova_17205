package calculator;

import java.io.StringReader;
import java.util.Scanner;

public class StringWordParser implements WordParser {
    Scanner scanner;

    public StringWordParser(String text){
        scanner= new Scanner(new StringReader(text));
    }
    @Override
    public String nextWord()  {
        if(scanner.hasNext()){
            return scanner.next();
        }
        scanner.close();
        return null;
    }
}
