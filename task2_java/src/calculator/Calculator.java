package calculator;

import calculator.exceptions.CommandException;
import calculator.exceptions.GetCommandNameException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.*;

public class Calculator {
    final static Logger logger = LogManager.getLogger(Calculator.class);

    public static class CalculatorContext{
        private Stack<Double> stack = new Stack<>();
        private Map<String, Double> vars = new HashMap<>();
        private Writer wr;

        public CalculatorContext(Writer wr){
            this.wr = wr;
        }

        // работа со стеком
        public void stackPush(double value){
            stack.push(value);
        }

        public double stackPop() {
            return stack.pop();
        }

        public double stackTop() {
            return stack.peek();
        }

        public int stackSize() {
            return stack.size();
        }

        //работа с переменными
        public void setVar(String name, double value) {
            vars.put(name, value);
        }

        public double getVar(String name) {
            return vars.get(name);
        }

        public boolean containsVar(String name) {
            return vars.containsKey(name);
        }

        //работа с потоком
        public Writer getWriter() {
            return wr;
        }
    }

    public void run(Reader reader, Writer writer) throws IOException{
        logger.info("Calculator run: enter ");
        BufferedReader bufferedReader = new BufferedReader(reader);
        CalculatorContext context = new CalculatorContext(writer);

        String line;
        while ((line = bufferedReader.readLine())!=null) {
            logger.info("Read line: " + line);
            if(line.isEmpty() || line.charAt(0)=='#') {
                continue;
            }
            String name;
            List<String> args = new ArrayList<>();

            try {
                // разбиваем line на слова
                WordParser parser= new StringWordParser(line);
                if ((name =parser.nextWord())==null) {
                    throw new GetCommandNameException();
                }
                String  word;
                while ((word =parser.nextWord())!=null)
                {
                    args.add(word);
                }
                Command cmd= CommandFactory.getInstance().getCommand(name);
                cmd.execute(context, args.toArray(new String[args.size()]));
            }
            catch (CommandException ex) {
                logger.error("Command error",ex);
                writer.write(String.format("Command error: '%s'", ex.toString()));
                writer.write(System.lineSeparator());
                writer.flush();
            }
        }
        logger.info("Calculator run: exit ");
    }
}
