package calculator;

public interface WordParser {
    String nextWord();
}
