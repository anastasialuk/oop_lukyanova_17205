package calculator.commands;

import calculator.Calculator;
import calculator.Command;
import calculator.exceptions.CommandException;
import calculator.exceptions.NotEnoughElementsInStackException;
import calculator.exceptions.WriteException;

import java.io.IOException;

public class PrintCommand implements Command {
    @Override
    public void execute(Calculator.CalculatorContext context, String[] args) throws CommandException {
        if (context.stackSize() < 1) {
            throw new NotEnoughElementsInStackException();
        }

        try {
            context.getWriter().write(Double.toString(context.stackTop()));
            context.getWriter().write(System.lineSeparator());
            context.getWriter().flush();
        } catch (IOException e) {
            throw new WriteException();
        }
    }
}
