package calculator.commands;

import calculator.Calculator;
import calculator.Command;
import calculator.exceptions.CommandException;
import calculator.exceptions.NotEnoughArgumentsException;

public class PushCommand implements Command{
    @Override
    public void execute(Calculator.CalculatorContext context, String[] args) throws CommandException {
        if (args.length < 1) {
            throw new NotEnoughArgumentsException();
        }
        if (context.containsVar(args[0])) {
            context.stackPush(context.getVar(args[0]));
        }
        else {
            context.stackPush(CommandUtils.convertToDouble(args[0]));
        }

    }
}
