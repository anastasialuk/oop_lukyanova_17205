package calculator.commands;

import calculator.Calculator;
import calculator.Command;
import calculator.exceptions.CommandException;
import calculator.exceptions.NotEnoughElementsInStackException;

public class PlusCommand implements Command {
    @Override
    public void execute(Calculator.CalculatorContext context, String[] args) throws CommandException {
        if (context.stackSize() < 2) {
            throw new NotEnoughElementsInStackException();
        }

        context.stackPush(context.stackPop() + context.stackPop());
    }

}
