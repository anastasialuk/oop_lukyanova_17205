package calculator.commands;

import calculator.exceptions.ConvertStrToDoubleException;

public class CommandUtils  {
    public static double convertToDouble(String str) throws ConvertStrToDoubleException {
        try
        {
            return Double.parseDouble(str);
        }
        catch (NumberFormatException ex)
        {
            throw new ConvertStrToDoubleException(ex);
        }
    }
}
