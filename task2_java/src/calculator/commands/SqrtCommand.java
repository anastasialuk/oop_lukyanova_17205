package calculator.commands;

import calculator.Calculator;
import calculator.Command;
import calculator.exceptions.CommandException;
import calculator.exceptions.NotEnoughElementsInStackException;
import calculator.exceptions.SqrtFromNegativeValueException;

public class SqrtCommand implements Command {
    @Override
    public void execute(Calculator.CalculatorContext context, String[] args) throws CommandException {
        if (context.stackSize() < 1) {
            throw new NotEnoughElementsInStackException();
        }
        if (context.stackTop() < 0)
        {
            throw new SqrtFromNegativeValueException();
        }

        context.stackPush(Math.sqrt(context.stackPop()));
    }
}
