package calculator.commands;

import calculator.Calculator;
import calculator.Command;
import calculator.exceptions.CommandException;
import calculator.exceptions.IncorrectVarNameException;
import calculator.exceptions.NotEnoughArgumentsException;

public class DefineCommand implements Command {
    @Override
    public void execute(Calculator.CalculatorContext context, String[] args) throws CommandException {
        if (args.length < 2) {
            throw new NotEnoughArgumentsException();
        }
        if (args[0].isEmpty() || !Character.isLetter(args[0].charAt(0))) {
            throw new IncorrectVarNameException();
        }
        context.setVar(args[0], CommandUtils.convertToDouble(args[1]));

    }
}
