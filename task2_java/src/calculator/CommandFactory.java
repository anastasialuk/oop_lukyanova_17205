package calculator;

import calculator.exceptions.CommandException;
import calculator.exceptions.CommandNotFoundException;
import calculator.exceptions.UnableToCreateCommandException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class CommandFactory {
    final static Logger logger = LogManager.getLogger(CommandFactory.class);

    private static volatile CommandFactory instance;
    private Map<String,Command> commandMap = new HashMap<>();

    private CommandFactory() throws IOException, UnableToCreateCommandException {
        logger.info("CommandFactory initialization: enter");
        Properties config = new Properties();
        config.load(CommandFactory.class.getResourceAsStream("commands.config"));
        for(String name : config.stringPropertyNames()){
            logger.info("CommandFactory create command: "+ name);
            try {
                Class cl = Class.forName(config.getProperty(name));
                commandMap.put(name, (Command) cl.getDeclaredConstructor().newInstance());
            }
            catch (Exception ex){
                logger.error("Unable to create command: " + name,ex);
                throw new UnableToCreateCommandException(ex);
            }
        }
        logger.info("CommandFactory initialization: exit");
    }

    public static CommandFactory getInstance() throws IOException, UnableToCreateCommandException {
        logger.info("Getting instance of CommandFactory ");
        if(instance == null){
            synchronized (CommandFactory.class){
                if(instance == null){
                    logger.info("Creating CommandFactory object ");
                    instance = new CommandFactory();
                }
            }
        }
        return instance;
    }

   public Command getCommand(String name) throws CommandException {
       logger.info("Getting command with name " + name);
       if(!commandMap.containsKey(name)){
            throw new CommandNotFoundException();
        }
        return commandMap.get(name);
    }
}
