import calculator.Calculator;
import calculator.Command;
import calculator.CommandFactory;

import java.io.*;

public class Main {

    public static  void main(String[] args){
        Calculator calculator = new Calculator();
        try {
            InputStream in;
            OutputStream out;
            if(args.length == 0) {
                in = System.in;
                out = System.out;
            }
            else if(args.length == 2){
                in = new FileInputStream(args[0]);
                out = new FileOutputStream(args[1]);
            }
            else {
                System.err.println("Wrong number of arguments!");
                return;
            }
            try(InputStreamReader reader = new InputStreamReader(in);
                OutputStreamWriter writer = new OutputStreamWriter(out)) {
                calculator.run(reader,writer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
