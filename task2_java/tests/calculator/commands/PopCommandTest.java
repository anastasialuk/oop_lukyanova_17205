package calculator.commands;

import calculator.Calculator;
import calculator.exceptions.CommandException;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;

public class PopCommandTest {

    @Test
    public void execute() throws CommandException {
        Writer wr = new StringWriter();
        Calculator.CalculatorContext context = new Calculator.CalculatorContext(wr);
        PopCommand pop = new PopCommand();
        String[] args = new String[0];
        context.stackPush(10);
        assertEquals(1.0,context.stackSize(),0.01);
        pop.execute(context, args);
        assertEquals(0.0, context.stackSize(), 0.01);
    }
}