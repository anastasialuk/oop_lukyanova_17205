package calculator.commands;

import calculator.Calculator;
import calculator.exceptions.CommandException;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;

public class DefineCommandTest {

    @Test
    public void execute() throws CommandException {
        Writer wr = new StringWriter();
        Calculator.CalculatorContext context = new Calculator.CalculatorContext(wr);
        DefineCommand define = new DefineCommand();
        String[] args = new String[]{"a","4"};
        define.execute(context, args);
        assertEquals(4.0, context.getVar("a"), 0.01);
    }
}