package calculator.commands;

import calculator.Calculator;
import calculator.exceptions.CommandException;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import static org.junit.Assert.*;

public class MultiplyCommandTest {

    @Test
    public void execute() throws CommandException {
        Writer wr = new StringWriter();
        Calculator.CalculatorContext context = new Calculator.CalculatorContext(wr);
        MultiplyCommand multiply = new MultiplyCommand();
        String[] args = new String[0];
        context.stackPush(4.0);
        context.stackPush(6.0);
        multiply.execute(context, args);
        assertEquals(24,context.stackTop(),0.01);
    }
}