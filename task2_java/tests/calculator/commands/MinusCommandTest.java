package calculator.commands;

import calculator.Calculator;
import calculator.exceptions.CommandException;
import org.junit.Test;

import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;

public class MinusCommandTest {

    @Test
    public void execute() throws CommandException {
        Writer wr = new StringWriter();
        Calculator.CalculatorContext context = new Calculator.CalculatorContext(wr);
        MinusCommand minus = new MinusCommand();
        String[] args = new String[0];
        context.stackPush(4.0);
        context.stackPush(6.0);
        minus.execute(context, null);
        assertEquals(2,context.stackTop(),0.01);
    }
}