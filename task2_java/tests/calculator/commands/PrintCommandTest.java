package calculator.commands;

import calculator.Calculator;
import calculator.exceptions.CommandException;
import org.junit.Test;

import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;

import static org.junit.Assert.*;

public class PrintCommandTest {

    @Test
    public void execute() throws CommandException {
        StringWriter wr = new StringWriter();
        Calculator.CalculatorContext context = new Calculator.CalculatorContext(wr);
        PrintCommand print = new PrintCommand();
        String[] args = new String[]{"a","4"};
        context.stackPush(10);
        print.execute(context, args);
        assertEquals(1, context.stackSize());
        assertEquals("10.0"+System.lineSeparator(), wr.getBuffer().toString());
    }
}