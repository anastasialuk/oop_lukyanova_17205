#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
#include "gtest/gtest.h"
#include "TritSet.h"

TEST(TestCaseName, TritSet_Empty) {
	TritSet set;
	EXPECT_EQ(set.length(), 0);
	EXPECT_EQ(set.capacity(), 0);
}

TEST(TestCaseName, TritSet_LengthCapacity) {
	TritSet set;
	// проверяем заполнение значением Trit::Unknown
	for (int i = 0; i < 10; i++) {
		set[i] = Trit::Unknown;
	}
	EXPECT_EQ(set.length(), 0);
	EXPECT_EQ(set.capacity(), 0);

	// проверяем заполнение значением != Trit::Unknown
	int i = 0;
	for (i; i < 10; i++) {
		set[i] = Trit::False;
	}
	EXPECT_EQ(set.length(), i);
	EXPECT_EQ(set.capacity(), i * 2 / 32 + 1);

	set[1000000] = Trit::Unknown;
	EXPECT_EQ(set.length(), i);
	EXPECT_EQ(set.capacity(), i * 2 / 32 + 1);

	set[1000000] = Trit::True;
	EXPECT_EQ(set.length(), 1000000 + 1);
	EXPECT_EQ(set.capacity(), 1000000 * 2 / 32 + 1);

	set[1000000] = Trit::Unknown;
	EXPECT_EQ(set.length(), i);
	EXPECT_EQ(set.capacity(), 1000000 * 2 / 32 + 1);

	// проверяет конструктор, принимающий начальное кол-во тритов
	TritSet set2(1000);
	EXPECT_EQ(set2.length(), 0);
	EXPECT_EQ(set2.capacity(), 1000 * 2 / 32 + 1);

	// проверяет конструктор копирования 
	TritSet set3(set2);
	EXPECT_EQ(set3.length(), 0);
	EXPECT_EQ(set3.capacity(), 1000 * 2 / 32 + 1);
}

TEST(TestCaseName, TritSet_Shrink) {
	TritSet set;

	set[1000000] = Trit::True;
	EXPECT_EQ(set.length(), 1000000 + 1);
	EXPECT_EQ(set.capacity(), (1000000 + 1) * 2 / 32 + 1);

	set[1000] = Trit::False;
	EXPECT_EQ(set.length(), 1000000 + 1);
	EXPECT_EQ(set.capacity(), (1000000 + 1) * 2 / 32 + 1);
	
	set[1000000] = Trit::Unknown;
	EXPECT_EQ(set.length(), 1000 + 1);
	EXPECT_EQ(set.capacity(), (1000000 + 1) * 2 / 32 + 1);
	
	set.shrink();
	EXPECT_EQ(set.length(), 1000 + 1);
	EXPECT_EQ(set.capacity(), (1000 + 1) * 2 / 32 + 1);
}

TEST(TestCaseName, TritSet_Trim) {
	TritSet set;

	set[1000000] = Trit::True;
	EXPECT_EQ(set.length(), 1000000 + 1);
	EXPECT_EQ(set.capacity(), (1000000 + 1) * 2 / 32 + 1);

	set[1000] = Trit::False;
	EXPECT_EQ(set.length(), 1000000 + 1);
	EXPECT_EQ(set.capacity(), (1000000 + 1) * 2 / 32 + 1);

	set.trim(10000);
	EXPECT_EQ(set.length(), 10000);
	EXPECT_EQ(set.capacity(), 10000 * 2 / 32 + 1);
}

TEST(TestCaseName, TritSet_OperatorSquareBrackets) 
{
	TritSet set;

	// проверяем заполнение значением != Trit::Unknown
	int i = 0;
	for (i = 0; i < 10; i++) 
	{
		set[i] = Trit::False;
	}

	for (i = 0; i < 10; i++)
	{
		EXPECT_EQ(set[i],Trit::False);
	}

	TritSet set1;
	for (i = 0; i < 10; i++)
	{
		EXPECT_EQ(set1[i], Trit::Unknown);
	}

	for (i = 0; i < 10; i++)
	{
		set1[i] = set[i];
	}

	for (i = 0; i < 10; i++)
	{
		EXPECT_EQ(set1[i], Trit::False);
	}
}

TEST(TestCaseName, TritSet_OperatorOR)
{
	TritSet set;

	// проверяем заполнение значением != Trit::Unknown
	int i = 0;

	for (i = 0; i < 10; i++)
	{
		set[i] = Trit::False;
	}

	TritSet set1;
	set1[0] = Trit::True;
	set1[1] = Trit::False;
	EXPECT_EQ(set1.length(), 2);

	set1 |= set;
	EXPECT_EQ(set1.length(), 2);
	EXPECT_EQ(set1[0], Trit::True);
	EXPECT_EQ(set1[1], Trit::False);


	for (i = 2; i < 10; i++)
	{
		EXPECT_EQ(set1[i], Trit::Unknown);
	}
}

TEST(TestCaseName, TritSet_OperatorAND)
{
	TritSet set;
	// проверяем заполнение значением != Trit::Unknown
	int i = 0;

	for (i = 0; i < 10; i++)
	{
		set[i] = Trit::False;
	}

	TritSet set1;
	set1[0] = Trit::True;
	set1[1] = Trit::False;
	EXPECT_EQ(set1.length(), 2);

	set1 &= set;
	EXPECT_EQ(set1.length(), 10);
	EXPECT_EQ(set1[0], Trit::False);
	EXPECT_EQ(set1[1], Trit::False);


	for (i = 2; i < 10; i++)
	{
		EXPECT_EQ(set1[i], Trit::False);
	}
}
TEST(TestCaseName, TritSet_OperatorNOT)
{
	TritSet set;

	set[1] = Trit::True;
	set[2] = Trit::False;
	
	EXPECT_EQ(set.length(), 3);
	TritSet set1 = ~set;

	EXPECT_EQ(set1.length(), 3);
	EXPECT_EQ(set1[0], Trit::Unknown);
	EXPECT_EQ(set1[1], Trit::False);
	EXPECT_EQ(set1[2], Trit::True);

}