#include "Trit.h"

Trit operator~(Trit value)
{
	switch (value)
	{
	case Trit::False:
		return Trit::True;
	case Trit::True:
		return Trit::False;
	}
	return Trit::Unknown;
}

Trit operator&(Trit left, Trit right)
{
	switch (left)
	{
	case Trit::False:
		return Trit::False;
	case Trit::True:
		return right;
	}
	if (right == Trit::False) 
	{
		return Trit::False;
	} 
	return Trit::Unknown;
}

Trit operator|(Trit left, Trit right)
{
	switch (left)
	{
	case Trit::False:
		return right;
	case Trit::True:
		return Trit::True;
	}
	if (right == Trit::True)
	{
		return Trit::True;
	}
	return Trit::Unknown;
}
