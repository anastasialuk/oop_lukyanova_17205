#include "TritSet.h"

namespace 
{
	void setTrit(uint32_t &intValue, size_t tritIndex, Trit tritValue) 
	{
		intValue = intValue & ~(0b11U << (tritIndex * 2)) | (static_cast<size_t>(tritValue) << (tritIndex * 2));
	}

	Trit getTrit(uint32_t intValue, size_t tritIndex) 
	{
		return static_cast<Trit>((intValue >> (tritIndex * 2)) & 0b11U);
	}

}

void TritSet::resize(size_t index, bool allowTrim)
{
	size_t newCapacity = index / TritsPerUint32 + 1;
	if (_capacity < newCapacity || (allowTrim && _capacity > newCapacity))
	{
		uint32_t *newArr = new uint32_t[newCapacity];
		if (_capacity < newCapacity)
		{
			std::copy(_tritArr, _tritArr + _capacity, newArr); // �������� ������ ������ � ����� ������
			std::fill(newArr + _capacity, newArr + newCapacity, 0); // ��������� ������� ������ ������� ������(Unknown)
		}
		else
		{
			std::copy(_tritArr, _tritArr + newCapacity, newArr); // �������� ����� ������� ������� � ����� ������
			size_t tritIndex = index % TritsPerUint32;
			while (tritIndex < TritsPerUint32) //��������� ������� ������ ���������� �������� ������� ��������� Unknown
			{
				setTrit(newArr[newCapacity-1], tritIndex, Trit::Unknown);
				tritIndex++;
			}
		}
		
		delete[] _tritArr;
		_tritArr = newArr;
		_capacity = newCapacity;
	}
	
}

Trit TritSet::get(size_t index) const
{
	if (index >= _length) 
	{
		return Trit::Unknown;
	}
	size_t  arrIndex = index/TritsPerUint32;
	size_t  tritIndex = index%TritsPerUint32;

	
	return getTrit(_tritArr[arrIndex], tritIndex);
}

void TritSet::set(size_t index, Trit value)
{
	if (index >= _length && value == Trit::Unknown)
	{
		return;
	}

	resize(index);
	size_t  arrIndex = index / TritsPerUint32;
	size_t  tritIndex = index % TritsPerUint32;
	Trit oldValue = getTrit(_tritArr[arrIndex], tritIndex);
	
	if (oldValue != value) 
	{
		setTrit(_tritArr[arrIndex], tritIndex, value);
		//update counters
		if(oldValue == Trit::True)
		{
			_trueCount--;
		}
		else if (oldValue == Trit::False)
		{
			_falseCount--;
		}

		if (value == Trit::True)
		{
			_trueCount++;
		}
		else if (value == Trit::False)
		{
			_falseCount++;
		}
	}


	if(index >= _length)
	{
		_length = index + 1;
	}

	else if( _length > 0 && index == (_length -1) && value == Trit::Unknown)
	{

		while (_length > 0)
		{
			if(get(_length -1) != Trit::Unknown)
			{
				break;
			}
			_length--;
		}
	}
	
}

void TritSet::copy(const TritSet & other)
{
	_length = other._length;
	_capacity = other._capacity;
	_tritArr = nullptr;
	if (_capacity > 0)
	{
		_tritArr = new uint32_t[_capacity];
		std::copy(other._tritArr, other._tritArr + _capacity, _tritArr);
	}

	_trueCount = other._trueCount;
	_falseCount = other._falseCount;

} 

void TritSet::move(TritSet & other)
{
	_length = other._length;
	other._length = 0;
	_capacity = other._capacity;
	other._capacity = 0;
	_tritArr = other._tritArr;
	other._tritArr = nullptr;

	_trueCount = other._trueCount;
	other._trueCount = 0;
	_falseCount = other._falseCount;
	other._falseCount = 0;
}

void TritSet::clear()
{
	delete[] _tritArr;
}

TritSet::Reference::Reference(TritSet *tritSet, size_t index)           // private init constructor
	: _tritSet(tritSet), _index(index)
{
}

TritSet::Reference::operator Trit() const
{
	return _tritSet->get(_index);
}

TritSet::Reference& TritSet::Reference::operator= (Trit value)                // assign Trit
{
	_tritSet->set(_index, value);
	return *this;
}

TritSet::Reference& TritSet::Reference::operator= (const Reference& other)       // assign Ref
{
	_tritSet->set(_index, (Trit)other);
	return *this;
}

TritSet::TritSet() // default constructor
{
}

TritSet::TritSet(size_t count)
{
	resize(count);
}

TritSet::TritSet(const TritSet& other) // ����������� �����������
{
	copy(other);
}

TritSet::TritSet(TritSet&& other)  // ����������� �����������
{
	move(other);
}

TritSet& TritSet::operator=(const TritSet& other) // �������� ������������ ������������ (copy assignment)
{
	if (this == &other)
	{
		return *this;
	}
	clear();
	copy(other);
	return *this;
}

TritSet& TritSet::operator=(TritSet&& other) // �������� ������������ ������������ (move assignment)
{
	if (this == &other)
	{
		return *this;
	}
	clear();
	move(other);
	return *this;
}

TritSet::~TritSet()
{
	clear();
}

bool TritSet::empty() const
{
	return _length == 0;
}

size_t TritSet::length() const
{
	return _length;
}

size_t TritSet::capacity() const
{
	return _capacity;
}

void TritSet::shrink()
{
	resize(_length, true);
}

void TritSet::trim(size_t lastIndex)
{
	if (lastIndex >= _length)
	{
		return;
	}
	// ������������� ���-�� ������ 
	_trueCount = 0;
	_falseCount = 0;
	for(size_t i =0; i < lastIndex; i++)
	{
		Trit t = get(i);
		if (t == Trit::True)
		{
			_trueCount++;
		}
		else if (t == Trit::False)
		{
			_falseCount++;
		}
	}
	resize(lastIndex, true);
	_length = lastIndex;
}

size_t TritSet::cardinality(Trit value) const
{
	switch (value)
	{
	case Trit::True:
		return _trueCount;
	case Trit::False:
		return _falseCount;
	case Trit::Unknown:
		return _length - _trueCount - _falseCount;
	}
	return 0;
}

std::unordered_map< Trit, size_t > TritSet::cardinality() const
{
	return std::unordered_map< Trit, size_t > {
		{Trit::True, _trueCount},
		{ Trit::False, _falseCount },
		{ Trit::Unknown, _length - _trueCount - _falseCount }
	};
}

Trit TritSet::operator[](size_t index) const
{
	return get(index);
}

TritSet::Reference TritSet::operator[](size_t index)
{
	return Reference(this, index);
}

TritSet & TritSet::operator|=(const TritSet & right)
{
	for (size_t i = 0; i < std::max(_length, right._length); ++i) {
		set(i, get(i) | right.get(i));
	}
	return *this;
}

TritSet & TritSet::operator&=(const TritSet & right)
{
	for (size_t i = 0; i < std::max(_length, right._length); ++i) {
		set(i, get(i) & right.get(i));
	}
	return *this;
}

TritSet operator~(const TritSet & set)
{
	TritSet tmp(set.length());
	for (size_t i = 0; i < set.length(); ++i) {
		tmp[i] = ~set[i];
	}
	return tmp;
}

TritSet operator|(const TritSet & left, const TritSet & right)
{
	TritSet tmp(left);
	return tmp |= right;
}

TritSet operator&(const TritSet & left, const TritSet & right)
{
	TritSet tmp(left);
	return tmp &= right;
}
