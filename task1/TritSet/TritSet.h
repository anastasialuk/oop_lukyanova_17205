#pragma once
#include "Trit.h"

#include <algorithm> 
#include <cstdint>
#include <unordered_map>


class TritSet
{
	uint32_t *_tritArr = nullptr; // ������ ��� �������� ������
	size_t _length = 0; // ���������� ������
	size_t _capacity = 0; // ���������� uint32_t, �������������� ��� �������� ������

	size_t _trueCount = 0; // ���-�� ������ �� ��������� true
	size_t _falseCount = 0; // ���-�� ������ �� ��������� false
	static const size_t TritsPerUint32 = 16; // ���-�� ������, �������� � ���� uint32_t

	// �������� ��������� �� ������� other � ������� ������ 
	// ������������ � ������������ ����������� � � ��������� ��������������� ������������
	void copy(const TritSet & other); 

	// ��������� ��������� �� ������� other � ������� ������ 
	// ������������ � ������������ ����������� � � ��������� ��������������� �����������
	void move(TritSet & other);

	// �������� ��������� �������� ������� 
	// ������������ � ��������� ��������������� ������������, ��������� ��������������� ������������ � � �����������
	void clear();

	// ������ ������ ���������� ������ 
	// ������������ � ������� set, shrink, trim � � ������������ TritSet(size_t)
	void resize(size_t index, bool allowTrim = false);

	// ���������� �������� ����� �� �������
	Trit get(size_t index) const;

	// ������ �������� ����� �� ������� �� value
	void set(size_t index, Trit value);

public:
	class Reference
	{
		friend class TritSet;
		TritSet	*_tritSet = nullptr;
		size_t _index = 0;

		//  ����������� ���������������� ���� ������
		Reference(TritSet *tritSet, size_t index);       
	
	public:

		operator Trit() const; // �������������� � ���� ����

		Reference& operator= (Trit value); // �������� ������������ ������ �������� ���� Trit
		
		Reference& operator= (const Reference& other); // �������� ������������ ������ �������� ���� Reference
	};

	TritSet(); // default constructor

	TritSet(size_t count); // �����������, ���������� ������ ��� count ������ 

	TritSet(const TritSet& other); // ����������� �����������
	
	TritSet(TritSet&& other); // ����������� �����������

	TritSet& operator=(const TritSet& other); // �������� ������������ ������������ (copy assignment)
	
	TritSet& operator=(TritSet&& other); // �������� ������������ ������������ (move assignment)
	
	~TritSet(); 

	bool empty() const; // ���������� True  ���� TritSet ������

	size_t length() const; // ���������� ������ ���������� �� Unknown ����� +1

	size_t capacity() const; // ���������� ������ ����������� ��������� (���-�� uint32_t)

	//������������ ������ �� �������� ������������ ��� �������� ���������� �������������� �����
	void shrink(); 

	//������������ ������ �� �������� ������������ ��� �������� ����� � �������� lastIndex
	void trim(size_t lastIndex); 

	// ���������� ���-�� ������ �� ��������� value
	size_t cardinality(Trit value) const;

	// ���������� ���� � ���-��� ������ ��� ���� ��������� ��������
	std::unordered_map< Trit, size_t > cardinality() const;

	// �������� ������� �� ������� ��� ������������ �������
	Trit operator[](size_t index) const;

	// �������� ������� �� ������� ���  �� ������������ �������
	Reference operator[](size_t index);

	TritSet & operator |= (const TritSet &right); // ��������  OR= (������ ����� �����)
	TritSet & operator &= (const TritSet &right); // ��������  AND= (������ ����� �����)
};

TritSet operator ~(const TritSet &set); // ��������  NOT (��������� ����� ��������)
TritSet operator | (const TritSet &left, const TritSet &right); // ��������  OR (��������� ����� ��������)
TritSet operator & (const TritSet &left, const TritSet &right); // ��������  AND (��������� ����� ��������)
