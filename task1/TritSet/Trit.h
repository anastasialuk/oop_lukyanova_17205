#pragma once

enum class Trit
{
	Unknown = 0, False = 1, True = 2
};

Trit operator ~ (Trit value);
Trit operator & (Trit left, Trit right);
Trit operator | (Trit left, Trit right);
