#include <string>
#include <cctype>
#include <vector> 
#include <algorithm>

#include "WordCounter.h"

void WordCounter::readFile(std::istream & in)
{
	std::string line;

	while (std::getline(in, line))
	{
		size_t startIndex = 0;

		for (size_t i = 0; i < line.length(); i++) 
		{
			if (!std::isalnum((unsigned char)line[i]))
			{
				if (i > startIndex)
				{
					wordMap[line.substr(startIndex, i - startIndex)]++;
					allWordCounter++;
				}
				startIndex = i + 1;
			}
		}
		if (line.length() > startIndex) //���� ����� � ����� ������
		{
			wordMap[line.substr(startIndex)]++;
			allWordCounter++;
		}
	}

}

bool pairSort(const std::pair<std:: string, int>& one, const std::pair<std::string, int>& two) 
{ 
	if(two.second == one.second)
	{
		return (one.first < two.first);
	}
	
	return (two.second < one.second);
}

void WordCounter::writeFile(std::ostream & out)
{
	std::vector<std::pair<std::string, int>> wordsVector(wordMap.begin(), wordMap.end());
	std::sort(wordsVector.begin(), wordsVector.end(), pairSort);

	for (auto& item : wordsVector)
	{
		out << item.first << ',' << item.second << ',' << (double)item.second / allWordCounter * 100. << std::endl;
	}
}
