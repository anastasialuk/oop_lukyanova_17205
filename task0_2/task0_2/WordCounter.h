#pragma once
#include <map>
#include <string>
#include <istream>
#include <ostream>



class WordCounter
{
	int allWordCounter = 0;
	std::map<std::string, int> wordMap;
public:
	void readFile(std::istream & in);
	void writeFile(std::ostream & out);
};

