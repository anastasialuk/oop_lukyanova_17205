#include <iostream>
#include <fstream>

#include "WordCounter.h"

int main(int argc, char* argv[])
{
	if (argc != 3) 
	{
		std::cerr << "Invalid number of arguments! " << std::endl;
		return -1;
	}
	std::ifstream fIn(argv[1]);
	if (!fIn)
	{
		std::cerr << "Unable to open input file! " << std::endl;
		return -1;
	}
	std::ofstream fOut(argv[2]);
	if (!fOut)
	{
		std::cerr << "Unable to open output file! " << std::endl;
		return -1;
	}

	WordCounter counter; //��������� ������ WordCounter �� �����
	counter.readFile(fIn);
	counter.writeFile(fOut);

	return 0;
}