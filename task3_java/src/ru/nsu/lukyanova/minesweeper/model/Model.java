package ru.nsu.lukyanova.minesweeper.model;

import ru.nsu.lukyanova.minesweeper.util.Observer;

import java.util.*;

public class Model implements ru.nsu.lukyanova.minesweeper.util.Observable {

    public enum Level {
        SIMPLE, MIDDLE, HARD
    }
    public enum GameState{
        INIT, PLAY, WIN, LOSE
    }

    private Level level = Level.SIMPLE;
    private Field field;
    private GameState gameState = GameState.INIT;
    private Timer timer;
    private long time = 0;

    public Model(Level level) {
        this.level = level;
        switch (level) {
            case HARD:
                field = new Field(24, 14, 70);
                break;
            case MIDDLE:
                field = new Field(18, 10, 30);
                break;
            default:
                field = new Field(10, 8, 10);
        }
    }


    public Level getLevel() {
        return level;
    }
    public GameState getGameState(){ return gameState; }
    public Field getField() {
        return field;
    }
    public long getTime() { return time; }

    public void restart(Level level) {
        stopTimer();
        time = 0;
        gameState = GameState.INIT;
        level = level;
        switch (level) {
            case HARD:
                field = new Field(24, 14, 70);
                break;
            case MIDDLE:
                field = new Field(18, 10, 30);
                break;
            default:
                field = new Field(10, 8, 10);
        }
        notifyFieldChange();
        notifyTimeChange();
    }

    private void startTimer(){
        time = 0;
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                time++;
                notifyTimeChange();
            }
        },1000, 1000);
    }
    private void stopTimer(){
        if(timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static class Field {
        public enum CellState {
            ZERO, NUM1, NUM2, NUM3, NUM4, NUM5, NUM6, NUM7, NUM8, BOMB,
            OPENED, CLOSED, FLAGGED, BOMBED, NOBOMB;
            public CellState getNextNumber(){
                return CellState.values()[this.ordinal() + 1];
            }
        }

        public static class Cell {
            public CellState value = CellState.ZERO;
            public CellState status = CellState.CLOSED;
        }

        private int columns;
        private int rows;
        private int bombsCount;
        private int flaggedCells = 0;
        private int openedCells = 0;
        private boolean isBombed = false;
        private Cell[][] cells;
        private static Random random = new Random();

        public Field(int columns, int rows, int bombsCount) {
            this.columns = columns;
            this.rows = rows;
            this.bombsCount = bombsCount;
        }

        public int getColumns() {
            return columns;
        }
        public int getRows() {
            return rows;
        }
        public boolean isBombed() { return isBombed; }
        public int getFlaggedCells() { return flaggedCells; }
        public int getOpenedCells() { return openedCells; }
        public int getBombsCount() { return bombsCount; }


        private boolean isOutOfBounds(int x, int y){
            return x < 0 || x >= columns || y < 0 || y >= rows;
        }

        public CellState getCell(int x, int y) {
            if (isOutOfBounds(x, y)) {
                throw new IndexOutOfBoundsException("Wrong cell coordinates.");
            }
            if(cells == null){
                return CellState.CLOSED;
            }
            Cell cell = cells[x][y];
            if (cell.status == CellState.OPENED) {
                return cell.value;
            }
            return cell.status;
        }

        private void generateField(int xIgnore, int yIgnore) {
            this.cells = new Cell[columns][rows];
            for (int x = 0 ; x < columns; x++){
                for (int y = 0; y < rows; y++){
                    cells[x][y] = new Cell();
                }
            }
            int generatedBombs = 0;
            while (generatedBombs < this.bombsCount){
                int x = random.nextInt(columns);
                int y = random.nextInt(rows);
                if((x == xIgnore && y == yIgnore) || cells[x][y].value == CellState.BOMB){
                    continue;
                }
                cells[x][y].value = CellState.BOMB;
                generatedBombs++;
                updateCellsAround(x, y);
            }
        }

        private void updateCellsAround( int x, int y){
            for (int i = x-1; i <= x +1; i++){
                for (int j = y - 1; j <= y + 1; j++) {
                    if (isOutOfBounds(i, j) || (i == x && j == y)
                            || cells[i][j].value == CellState.BOMB) {
                        continue;
                    }
                    cells[i][j].value = cells[i][j].value.getNextNumber();
                }
            }
        }

        private boolean openCell(int x, int y) {
            if(isOutOfBounds(x, y)) {
                return false;
            }
            if(cells == null){
                generateField(x, y);
            }
            if(cells[x][y].status != CellState.CLOSED){
                return false;
            }
            if(cells[x][y].value == CellState.BOMB){
                cells[x][y].status = CellState.BOMBED;
                isBombed = true;
                openBombs();
            }
            else if(cells[x][y].value == CellState.ZERO){
                cells[x][y].status = CellState.OPENED;
                openedCells++;
                openNeighbors(x, y);
            }
            else {
                cells[x][y].status = CellState.OPENED;
                openedCells++;
            }
            return true;
        }

        private void openNeighbors(int x, int y) {
            openCell(x-1,y);
            openCell(x+1,y);
            openCell(x,y-1);
            openCell(x,y+1);
            openCell(x+1, y+1);
            openCell(x+1, y-1);
            openCell(x-1, y+1);
            openCell(x-1, y-1);
        }

        private void openBombs() {

            for (int i = 0 ; i < columns; i++){
                for (int j = 0; j < rows; j++){
                    switch (cells[i][j].status){
                        case OPENED:
                            continue;
                        case CLOSED:
                            if(cells[i][j].value == CellState.BOMB){
                                cells[i][j].status = CellState.OPENED;
                            }
                            break;
                        case FLAGGED:
                            if(cells[i][j].value != CellState.BOMB){
                                cells[i][j].status = CellState.NOBOMB;
                            }
                            break;
                    }
                }
            }
        }

        private boolean flagCell(int x, int y) {
            if(cells == null || isOutOfBounds(x, y) || flaggedCells >= bombsCount) {
                return false;
            }
            if(cells[x][y].status == CellState.CLOSED){
                cells[x][y].status = CellState.FLAGGED;
                flaggedCells++;
            }
            else if(cells[x][y].status == CellState.FLAGGED) {
                cells[x][y].status = CellState.CLOSED;
                flaggedCells++;
            }
            else {
                return false;
            }
            return true;
        }
    }

    public void openCell(int x, int y) {
        if(gameState == GameState.INIT){
            gameState = GameState.PLAY;
            startTimer();
        }
        if(gameState == GameState.PLAY && field.openCell(x, y)) {
            checkWinner();
            notifyFieldChange();
        }
    }

    public void flagCell(int x, int y) {
        if(gameState == GameState.PLAY && field.flagCell(x, y)) {
            notifyFieldChange();
        }
    }
    private void checkWinner(){
        if(gameState == GameState.PLAY){
            if(field.isBombed()){
                gameState = GameState.LOSE;
                stopTimer();
            }
            else if (field.getRows()*field.getColumns() == (field.getOpenedCells() + field.getFlaggedCells())) {
                gameState = GameState.WIN;
                stopTimer();
            }
        }
    }

    private List<Observer> observers = new LinkedList<>();
    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyTimeChange() {
        for(Observer o: observers){
            o.updateTime();
        }
    }

    @Override
    public void notifyFieldChange() {
        for(Observer o: observers){
            o.updateField();
        }
    }
}