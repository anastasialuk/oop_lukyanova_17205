package ru.nsu.lukyanova.minesweeper.consoleui;

import ru.nsu.lukyanova.minesweeper.Controller;
import ru.nsu.lukyanova.minesweeper.View;
import ru.nsu.lukyanova.minesweeper.model.Model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleView implements View {

    private Model model;
    private Controller controller;
    private int columns;
    private int rows;

    public ConsoleView(Model model, Controller controller) {
        this.model = model;
        this.controller = controller;
        this.columns = model.getField().getColumns();
        this.rows = model.getField().getRows();
    }

    @Override
    public void updateTime() {
    }

    @Override
    public void updateField() {
        printField();
        System.out.println("Flags: " +( model.getField().getBombsCount() - model.getField().getFlaggedCells()));
        if(model.getGameState() == Model.GameState.WIN){
            System.out.println("You won!");
        }
        else if(model.getGameState()== Model.GameState.LOSE){
            System.out.println("You lose!");
        }

    }

    @Override
    public void initView() {
        printField();
        new Thread(() -> {
            try(BufferedReader in = new BufferedReader(new InputStreamReader(System.in))) {
                while (model.getGameState() == Model.GameState.INIT || model.getGameState() == Model.GameState.PLAY) {
                    String line = in.readLine();
                    if (line.equalsIgnoreCase("exit")) {
                        System.out.println("Exit");
                        break;
                    }
                    String[] parts = line.split("\\s+");
                    if(parts.length == 3)
                    {
                        try {
                            int x = Integer.parseInt(parts[1]);
                            int y = Integer.parseInt(parts[2]);

                            if(parts[0].equalsIgnoreCase("o") || parts[0].equalsIgnoreCase("open")) {
                                model.openCell(x, y);
                                continue;
                            }
                            else if(parts[0].equalsIgnoreCase("f") || parts[0].equalsIgnoreCase("flag")) {
                                model.flagCell(x, y);
                                continue;
                            }
                        }
                        catch(NumberFormatException ex)
                        {
                            System.out.println("Wrong coordinates format!");
                            continue;
                        }
                    }
                    System.out.println("Wrong command!");

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    private void printField() {
        for (int y = 0; y < rows; y++) {
            for (int x = 0; x < columns; x++) {
                String ch = "_";
                switch (model.getField().getCell(x, y)) {
                    case NUM1:
                        ch = "1";
                        break;
                    case NUM2:
                        ch = "2";
                        break;
                    case NUM3:
                        ch = "3";
                        break;
                    case NUM4:
                        ch = "4";
                        break;
                    case NUM5:
                        ch = "5";
                        break;
                    case NUM6:
                        ch = "6";
                        break;
                    case NUM7:
                        ch = "7";
                        break;
                    case NUM8:
                        ch = "8";
                        break;
                    case BOMB:
                        ch = "*";
                        break;
                    case CLOSED:
                        ch = "#";
                        break;
                    case FLAGGED:
                        ch = "@";
                        break;
                    case BOMBED:
                        ch = "¤";
                        break;
                    case NOBOMB:
                        ch = "0";
                        break;

                }
                System.out.print(ch);
            }
            System.out.println("");
        }

    }
}