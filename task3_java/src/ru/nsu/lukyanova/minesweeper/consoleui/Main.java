package ru.nsu.lukyanova.minesweeper.consoleui;

import ru.nsu.lukyanova.minesweeper.Game;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        game.run(Game.GameType.CONSOLE);
    }
}
