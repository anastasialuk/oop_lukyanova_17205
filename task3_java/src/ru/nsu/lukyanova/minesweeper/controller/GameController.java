package ru.nsu.lukyanova.minesweeper.controller;

import ru.nsu.lukyanova.minesweeper.Controller;
import ru.nsu.lukyanova.minesweeper.View;
import ru.nsu.lukyanova.minesweeper.model.Model;

import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameController implements ru.nsu.lukyanova.minesweeper.Controller {
    private Model model;
    private View view;

    public GameController() {
    }

    @Override
    public void start() {
        view.initView();
    }

    @Override
    public void setModel(Model model) {
        this.model = model;
    }

    @Override
    public void setView(View view) {
        this.view = view;
    }

    @Override
    public void openCell(int x, int y) {
        model.openCell(x, y);
    }

    @Override
    public void flagCell(int x, int y) {
        model.flagCell(x, y);
    }

    @Override
    public void restart() {
        model.restart(model.getLevel());
    }

    @Override
    public void restart(Model.Level level) {
        model.restart(level);
    }
}