package ru.nsu.lukyanova.minesweeper;

import ru.nsu.lukyanova.minesweeper.util.Observer;

public interface View extends Observer {

    void initView();
}
