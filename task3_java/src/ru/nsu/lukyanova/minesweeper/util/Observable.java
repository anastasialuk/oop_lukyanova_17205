package ru.nsu.lukyanova.minesweeper.util;

public interface Observable {
    void addObserver(Observer o);
    void removeObserver(Observer o);
    void notifyTimeChange();
    void notifyFieldChange();
}
