package ru.nsu.lukyanova.minesweeper.util;

public interface Observer {
    void updateTime();
    void updateField();
}
