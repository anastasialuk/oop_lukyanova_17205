package ru.nsu.lukyanova.minesweeper.swingui;

import ru.nsu.lukyanova.minesweeper.Controller;
import ru.nsu.lukyanova.minesweeper.View;
import ru.nsu.lukyanova.minesweeper.controller.GameController;
import ru.nsu.lukyanova.minesweeper.model.Model;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Map;

public class SwingView extends JFrame implements View, MouseListener {

    private Model model;
    private Controller controller;
    private JPanel panel;
    private JLabel gameStatusLabel;
    private JLabel flagsCountLabel;
    private JLabel timeLabel;
    private int columns;
    private int rows ;
    public static final int CELL_SIZE = 50;
    private Map<Model.Field.CellState, Image> imageMap = new HashMap<>();

    public SwingView(Model model, Controller controller) {
        this.model = model;
        model.addObserver(this);
        this.controller = controller;

        initImages();
        initPanel();
        initStatusBar();
        initMenu();
        initFrame();
    }

    private void initPanel (){
        panel = new JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                for(int x = 0 ; x < columns; x++){
                    for(int y = 0; y < rows; y++){
                        g.drawImage(imageMap.get(model.getField().getCell(x, y)),x * CELL_SIZE,y * CELL_SIZE,this );
                    }
                }
            }
        };
        initSize();
        panel.addMouseListener(this);
        getContentPane().add(panel);
    }

    private void initSize(){
        this.columns = model.getField().getColumns();
        this.rows = model.getField().getRows();
        panel.setPreferredSize(new Dimension(columns * CELL_SIZE,rows * CELL_SIZE));
        this.pack();
    }

    private void initFrame() {
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("minesweeper");
        setIconImage(getImage("icon"));
        setLocationRelativeTo(null);
        setResizable(false);
    }
    private void initStatusBar(){
        JPanel statusBar = new JPanel(new BorderLayout());
        statusBar.setBorder(new CompoundBorder(new LineBorder(Color.DARK_GRAY),
                new EmptyBorder(4, 4, 4, 4)));
        gameStatusLabel = new JLabel();
        flagsCountLabel = new JLabel();
        flagsCountLabel.setText("Flags: " + model.getField().getBombsCount());
        timeLabel = new JLabel();
        timeLabel.setText("Time: " + model.getTime());
        statusBar.add(gameStatusLabel, BorderLayout.WEST);
        statusBar.add(flagsCountLabel, BorderLayout.CENTER);
        statusBar.add(timeLabel,BorderLayout.EAST);
        getContentPane().add(statusBar,BorderLayout.SOUTH);
    }

    private void initMenu(){
        JMenuBar jMenuBar = new JMenuBar();
        JMenu jMenu = new JMenu("File");
        jMenuBar.add(jMenu);

        JMenuItem jMenuItemRestart = new JMenuItem("Restart");
        jMenuItemRestart.addActionListener(e -> {
            controller.restart();
        });

        JMenuItem jMenuItemSimple = new JMenuItem("Simple");
        jMenuItemSimple.addActionListener(e -> {
            controller.restart(Model.Level.SIMPLE);
            initSize();
        });

        JMenuItem jMenuItemMiddle = new JMenuItem("Middle");
        jMenuItemMiddle.addActionListener(e -> {
            controller.restart(Model.Level.MIDDLE);
            initSize();
        });

        JMenuItem jMenuItemHard = new JMenuItem("Hard");
        jMenuItemHard.addActionListener(e -> {
            controller.restart(Model.Level.HARD);
            initSize();
        });

        JMenuItem jMenuItemExit = new JMenuItem("Exit");
        jMenuItemExit.addActionListener(e -> {
            System.exit(0);
        });

        jMenu.add(jMenuItemRestart);
        jMenu.addSeparator();
        jMenu.add(jMenuItemSimple);
        jMenu.add(jMenuItemMiddle);
        jMenu.add(jMenuItemHard);
        jMenu.addSeparator();
        jMenu.add(jMenuItemExit);

        this.setJMenuBar(jMenuBar);

    }

    private void initImages(){
        for(Model.Field.CellState cellState : Model.Field.CellState.values()){
            imageMap.put(cellState, getImage(cellState.name().toLowerCase()));
        }
    }
    private Image getImage(String name){
        String filename = "/img/" + name + ".png";
        ImageIcon icon = new ImageIcon(getClass().getResource(filename));
        return icon.getImage();
    }

    @Override
    public void updateTime() {
        SwingUtilities.invokeLater(() -> {
            timeLabel.setText("Time: " + model.getTime());
        });
    }

    @Override
    public void updateField() {
        panel.repaint();

        SwingUtilities.invokeLater(() -> {
            if(model.getGameState() == Model.GameState.WIN){
                gameStatusLabel.setText("You won!");
            }
            else if(model.getGameState()== Model.GameState.LOSE){
                gameStatusLabel.setText("You lose!");
            }
            else {
                gameStatusLabel.setText("");
            }
            flagsCountLabel.setText("Flags: " +( model.getField().getBombsCount() - model.getField().getFlaggedCells()));
        });

    }

    @Override
    public void initView() {
        this.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(SwingUtilities.isLeftMouseButton(e)){
            controller.openCell(e.getX()/ SwingView.CELL_SIZE, e.getY()/ SwingView.CELL_SIZE);
        }
        else if(SwingUtilities.isRightMouseButton(e)){
            controller.flagCell(e.getX()/ SwingView.CELL_SIZE, e.getY()/ SwingView.CELL_SIZE);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}