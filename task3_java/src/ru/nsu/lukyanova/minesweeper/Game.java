package ru.nsu.lukyanova.minesweeper;

import ru.nsu.lukyanova.minesweeper.consoleui.ConsoleView;
import ru.nsu.lukyanova.minesweeper.model.Model;
import ru.nsu.lukyanova.minesweeper.controller.GameController;
import ru.nsu.lukyanova.minesweeper.swingui.SwingView;

public class Game {
    public enum GameType {
        SWING, CONSOLE
    }

    public void run(GameType gameType) {
        Model model = new Model(Model.Level.SIMPLE);
        GameController controller = new GameController();
        View view = gameType == GameType.SWING ? new SwingView(model, controller) : new ConsoleView(model, controller);
        model.addObserver(view);
        controller.setModel(model);
        controller.setView(view);
        controller.start();
    }

}
