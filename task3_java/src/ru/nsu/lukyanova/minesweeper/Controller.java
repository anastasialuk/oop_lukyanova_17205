package ru.nsu.lukyanova.minesweeper;

import ru.nsu.lukyanova.minesweeper.model.Model;

public interface Controller {
    void start();
    void setModel(Model model);
    void setView(View view);

    void openCell(int x, int y);

    void flagCell(int x, int y);

    void restart();

    void restart(Model.Level level);
}
