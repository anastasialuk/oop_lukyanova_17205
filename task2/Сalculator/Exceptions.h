#pragma once

#include <exception>

class CommandException : public std::exception
{
public:
	CommandException() = default;
	CommandException(char const* msg) : std::exception(msg) {}
};

class CommandNotFoundException : public CommandException 
{
public:
	CommandNotFoundException() : CommandException("Command not found") {}
};

class NotEnouthElementsInStackException : public CommandException 
{
public:
	NotEnouthElementsInStackException() : CommandException("Not enouth elements in stack") {}
};

class NotEnouthArgumentsException : public CommandException 
{
public:
	NotEnouthArgumentsException() : CommandException("Not enouth arguments") {}
};

class SqrtFromNegativeValueException : public CommandException
{
public:
	SqrtFromNegativeValueException() : CommandException("Unable to calulate sqrt from negative value") {}
};

class ConvertStrToDoubleExeption : public CommandException
{
public:
	ConvertStrToDoubleExeption() : CommandException("Unable to convert string to double") {}
};

class IncorrectVarNameException : public CommandException
{
public:
	IncorrectVarNameException() : CommandException("Incorrect variable name") {}
};

class VariableNotFoundException : public CommandException
{
public:
	VariableNotFoundException() : CommandException("Variable not found") {}
};

class GetCommandNameException : public CommandException
{
public:
	GetCommandNameException() : CommandException("Unable to get command name") {}
};