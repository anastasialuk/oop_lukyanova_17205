#pragma once
#include "Command.h"

class Plus : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;
};

class Minus : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;
};

class Multiply : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;

};

class Divide : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;

};

class Sqrt : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;

};

class Print : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;

};

class Pop : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;
};

class Push : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;
};

class Define : public Command
{
public:
	static const std::string Name;
	virtual void execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args) override;
};