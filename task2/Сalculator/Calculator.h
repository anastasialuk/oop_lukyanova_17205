#pragma once

#include <algorithm> 
#include <cstdint>
#include <stack>
#include <map>


class Calculator
{
public:
	class CalculatorContext
	{
	public:
		CalculatorContext(std::ostream &os) : os(os) {}

		// ������ �� ������
		void stackPush(double value);
		double stackPop();
		double stackTop() const;
		size_t stackSize() const;

		//������ � �����������
		void setVar(const std::string &name, double value);
		double getVar(const std::string &name) const;
		bool containsVar(const std::string &name) const; 

		//������ � �������
		std::ostream &getOstream() { return os; }
	private:
		std::stack<double> stack;
		std::map<std::string, double> vars;
		std::ostream &os;
	};

	void run(std::istream &input, std::ostream &output); // ��������� �����������
};



