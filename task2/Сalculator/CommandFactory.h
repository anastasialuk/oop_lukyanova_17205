#pragma once
#include "Command.h"

typedef Command*(*factoryMethod)(); // ���������� ������ �-��� ��� �������� �������
class CommandFactory 
{
	CommandFactory() = default;
	// ��������� ��� �������� ������������ ����� ������� � ��������� �� �������, ������� ������ �������
	std::map<std::string, factoryMethod> commands;
public:
	CommandFactory(const CommandFactory&) = delete;
	~CommandFactory() = default;

	static CommandFactory &getInstance()
	{
		static CommandFactory f;
		return f;
	}

	Command *getCommand(const std::string &name) 
	{
		// ���� ������� �� �����
		auto registeredPair = commands.find(name);

		if (registeredPair == commands.end())
			throw CommandNotFoundException();		  
		// ���������� ����� ��������� ������� 
		return registeredPair->second();
	}

	bool registerCommand(const std::string &name, factoryMethod createMethod)
	{
		// ��������� ����� �������
		auto result = commands.insert(std::make_pair(name, createMethod));
		// ���������� ���� �� ������� ���������
		return result.second;
	}
};

