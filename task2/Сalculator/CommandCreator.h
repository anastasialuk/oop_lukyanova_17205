#pragma once
#include "CommandFactory.h"

template<typename T>
class CommandCreator
{
public:
	// give derived classes the ability to create themselves
	static Command *create() {
		return new T();
	}
	CommandCreator() { isRegistered; } // force using static isRegistered
protected:

	static const bool isRegistered;
};

template<typename T>
// attempt to initialise the IsRegistered variable of derived classes
// whilst registering them to the factory
const bool CommandCreator<T>::isRegistered =
CommandFactory::getInstance().registerCommand(T::Name, &CommandCreator<T>::create);

