#include <string>
#include "Calculator.h"
#include "Command.h"
#include "CommandFactory.h"
#include "Parser.h"


void Calculator::run(std::istream & input, std::ostream & output)
{
	CalculatorContext context(output);

	std::string line;
	while (std::getline(input, line)) {
		if(line.empty() || line[0]=='#') {
			continue;
		}
		std::string name;
		std::vector<std::string> args;

		try
		{
			// ��������� line �� �����
			Parser parser(line);
			if (!parser.nextWord(name)) {
				throw GetCommandNameException();
			}
			std::string word;
			while (parser.nextWord(word))
			{
				args.push_back(word);
			}
			std::auto_ptr<Command> cmd(CommandFactory::getInstance().getCommand(name));
			cmd->execute(context, args);
		}
		catch (CommandException &ex) 
		{
			output << "Command '" << name << "' error: '" << ex.what() << "'" << std::endl;
		}
	}

}

void Calculator::CalculatorContext::stackPush(double value)
{
	stack.push(value);
}

double Calculator::CalculatorContext::stackPop()
{
	double top = stack.top();
	stack.pop();
	return top;
}

double Calculator::CalculatorContext::stackTop() const
{
	return stack.top();
}

size_t Calculator::CalculatorContext::stackSize() const
{
	return stack.size();
}

void Calculator::CalculatorContext::setVar(const std::string & name, double value)
{
	vars[name] = value;
}

double Calculator::CalculatorContext::getVar(const std::string & name) const
{
	auto it = vars.find(name);
	if (it == vars.end()) {
		throw VariableNotFoundException();
	}
	return it->second;
}

bool Calculator::CalculatorContext::containsVar(const std::string & name) const
{
	return vars.find(name) != vars.end();
}
