#include <iostream>
#include <math.h>
#include <string>
#include <cctype>

#include "Commands.h"
#include "CommandCreator.h"

namespace {
	double convertToDouble(const std::string &str) {
		double ret;
		try
		{
			ret = std::stod(str);
		}
		catch (...)
		{
			throw ConvertStrToDoubleExeption();
		}
		return ret; 
	}
	CommandCreator<Plus> plusCreator;
	CommandCreator<Minus> minusCreator;
	CommandCreator<Multiply> multCreator;
	CommandCreator<Divide> divCreator;
	CommandCreator<Define> defCreator;
	CommandCreator<Print> printCreator;
	CommandCreator<Sqrt> sqrtCreator;
	CommandCreator<Pop> popCreator;
	CommandCreator<Push> pushCreator;

} 

const std::string Plus::Name = "+";
void Plus::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 2) {
		throw NotEnouthElementsInStackException();
	}

	context.stackPush(context.stackPop() + context.stackPop());
}
const std::string Minus::Name = "-";
void Minus::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 2) {
		throw NotEnouthElementsInStackException();
	}

	context.stackPush(context.stackPop() - context.stackPop());
}
const std::string Multiply::Name = "*";
void Multiply::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 2) {
		throw NotEnouthElementsInStackException();
	}

	context.stackPush(context.stackPop() * context.stackPop());
}
const std::string Divide::Name = "/";
void Divide::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 2) {
		throw NotEnouthElementsInStackException();
	}

	context.stackPush(context.stackPop() / context.stackPop());
}
const std::string Sqrt::Name = "SQRT";
void Sqrt::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 1) {
		throw NotEnouthElementsInStackException();
	}
	if (context.stackTop() < 0) 
	{
		throw SqrtFromNegativeValueException();
	}

	context.stackPush(sqrt(context.stackPop()));
}
const std::string Print::Name = "PRINT";
void Print::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 1) {
		throw NotEnouthElementsInStackException();
	}

	context.getOstream() << context.stackTop() << std::endl;
}
const std::string Pop::Name = "POP";
void Pop::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (context.stackSize() < 1) {
		throw NotEnouthElementsInStackException();
	}
	context.stackPop();
}
const std::string Push::Name = "PUSH";
void Push::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (args.size() < 1) {
		throw NotEnouthArgumentsException();
	}
	if (context.containsVar(args[0])) {
		context.stackPush(context.getVar(args[0]));
	}
	else
	{
		context.stackPush(convertToDouble(args[0]));
	}
}

const std::string Define::Name = "DEFINE";
void Define::execute(Calculator::CalculatorContext & context, const std::vector<std::string> & args)
{
	if (args.size() < 2) {
		throw NotEnouthArgumentsException();
	}
	if (args[0].empty() || !std::isalpha(args[0][0])) {
		throw IncorrectVarNameException();
	}
	context.setVar(args[0], convertToDouble(args[1]));
}
