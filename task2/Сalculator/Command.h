#pragma once
#include <vector>

#include "Exceptions.h"
#include "Calculator.h"

class Command
{
public:
	virtual void execute(Calculator::CalculatorContext &context, const std::vector<std::string> & args) = 0;
	virtual ~Command() = default;
};

