#pragma once
#include <string>
#include <sstream>

class Parser
{ 
	std::istringstream istrstream;
public:
	Parser(const std::string & text);
	bool nextWord(std::string & word);
};

