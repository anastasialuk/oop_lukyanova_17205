#include "Calculator.h"
#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
	Calculator calc;
	calc.run(std::cin, std::cout);

}