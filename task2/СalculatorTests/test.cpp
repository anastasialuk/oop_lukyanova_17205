#define _SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
#include "gtest/gtest.h"
#include "Calculator.h"
#include <iostream>
#include "Commands.h"


TEST(TestCaseName, PUSH)
{
	Calculator::CalculatorContext context(std::cout);
	Push push;
	std::vector<std::string> args;
	args.push_back("10.5");
	push.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(10.5, context.stackTop());
}

TEST(TestCaseName, DEFINE)
{
	Calculator::CalculatorContext context(std::cout);
	Define define;
	std::vector<std::string> args;
	args.push_back("a");
	args.push_back("4");
	define.execute(context, args);
	EXPECT_EQ(4, context.getVar("a"));
}

TEST(TestCaseName, PRINT)
{
	std::ostringstream ostrstream;
	Calculator::CalculatorContext context(ostrstream);
	Print print;
	std::vector<std::string> args;
	context.stackPush(10);
	print.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ("10\n", ostrstream.str());
}

TEST(TestCaseName, POP)
{
	Calculator::CalculatorContext context(std::cout);
	Pop pop;
	std::vector<std::string> args;
	context.stackPush(10);
	EXPECT_EQ(1, context.stackSize());
	pop.execute(context, args);
	EXPECT_EQ(0, context.stackSize());
}

TEST(TestCaseName, PLUS)
{
	Calculator::CalculatorContext context(std::cout);
	Plus plus;
	std::vector<std::string> args;
	context.stackPush(10.5);
	context.stackPush(5.5);
	plus.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(16, context.stackTop());
}

TEST(TestCaseName, MINUS)
{
	Calculator::CalculatorContext context(std::cout);
	Minus minus;
	std::vector<std::string> args;
	context.stackPush(5.5);
	context.stackPush(10.5);
	minus.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(5, context.stackTop());
}

TEST(TestCaseName, MULTIPLY)
{
	Calculator::CalculatorContext context(std::cout);
	Multiply multiply;
	std::vector<std::string> args;
	context.stackPush(6);
	context.stackPush(5);
	multiply.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(30, context.stackTop());
}

TEST(TestCaseName, DIVIDE)
{
	Calculator::CalculatorContext context(std::cout);
	Divide divide;
	std::vector<std::string> args;
	context.stackPush(6);
	context.stackPush(36);
	divide.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(6, context.stackTop());
}

TEST(TestCaseName, SQRT)
{
	Calculator::CalculatorContext context(std::cout);
	Sqrt sqrt;
	std::vector<std::string> args;
	context.stackPush(225);
	sqrt.execute(context, args);
	EXPECT_EQ(1, context.stackSize());
	EXPECT_EQ(15, context.stackTop());
}
