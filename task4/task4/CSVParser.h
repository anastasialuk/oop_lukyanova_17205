#pragma once 

#include <tuple>
#include <vector>
#include <string>
#include <sstream>

class WrongNumberOfColumns : public std::exception {
	std::size_t row;
public:
	WrongNumberOfColumns(std::size_t row) : row(row),
		std::exception(("Wrong number of columns in row " + std::to_string(row)).c_str()) {
	}
	std::size_t errorRow() { return row; }
};

class WrongDataFormatOfColumn : public std::exception {
	std::size_t row;
	std::size_t column;
public:
	WrongDataFormatOfColumn(std::size_t line, std::size_t column) : row(line), column(column),
		std::exception(("Wrong format of data in row: " + std::to_string(line) + ", column: " + std::to_string(column)).c_str()) {
	}
	std::size_t errorRow() { return row; }
	std::size_t errorColumn() { return column; }
};

template <typename T>
struct StringParser {
	static T parse(const std::string & str, std::size_t row, std::size_t column) {
		if (str.empty())
			return T();
		// ��������������� ������ � ������ ��� � ������� ���������� ������ � ��������� >>
		std::istringstream stream(str);
		T t;
		stream >> t;

		if (stream.fail())
			// ���� �������������� �� ����� ������ ������
			throw WrongDataFormatOfColumn(row+1, column+1);

		return t;
	}
};

template <>
struct StringParser<std::string> {
	static std::string parse(const std::string & str, std::size_t, std::size_t) {
		return str;
	}
};

template<class Tuple, std::size_t N>
struct TupleParser {
	static void parse(std::vector<std::string> values, Tuple& t, std::size_t row)
	{
		TupleParser<Tuple, N - 1>::parse(values, t, row);

		std::get<N - 1>(t) = StringParser<std::tuple_element<N - 1, Tuple>::type>::parse(values[N - 1], row, N - 1);
	}
};

template<class Tuple>
struct TupleParser<Tuple, 1> {
	static void parse(std::vector<std::string> values, Tuple& t, std::size_t row)
	{
		std::get<0>(t) = StringParser<std::tuple_element<0, Tuple>::type>::parse(values[0], row, 0);
	}
};

template <typename ... TList>
class CSVParser {
	using row_type = std::tuple<TList ...>;

	std::istream &is_;
	size_t skipLines_ = 0;
	char separator_ = ',';
	char quote_ = '"';
	char escape_ = '\\';

	// ���������� ��������� ������, ��������� � �� �������
	// ���������� false � ������, ���� �� ������ ��������� ������
	bool parseNextRow(std::vector<std::string> &columns) {
		std::string line;
		//���������� ������ ������
		do {
			std::getline(is_, line);
		} while (is_ && line.empty());

		//��������� ��������� ������
		if (!is_)
			return false;
		
		// ������ ������
		bool is_escaped = false;
		bool quoted = false;

		std::string column;
		for (std::size_t i = 0; i < line.length(); ++i) {
			if (line[i] == escape_) {
				// ��������� escape ������
				is_escaped = true;
				continue;
			}

			if (!is_escaped && line[i] == quote_) {
				// ��������� quote ������
				quoted = !quoted;
				continue;
			}

			if (!is_escaped && !quoted &&
				(line[i] == separator_)) {
				// ��������� separator ������ (���������� ������� �������� � vector)
				columns.push_back(column);
				column.clear();
				continue;
			}
			column += line[i];
			if (is_escaped)
				is_escaped = false;
		}
		// ���������� ��������� ������� � vector
		columns.push_back(column);

		return true;
	}
public:
	class iterator {
		int rowNum;
		CSVParser &parser;
		row_type value;
		void parseTuple() {
			std::vector<std::string> columns;
			if (!parser.parseNextRow(columns)) {
				rowNum = -1;
				return;
			}
			if (columns.size() < sizeof...(TList))
				throw WrongNumberOfColumns(rowNum+1);
			TupleParser<row_type ,sizeof...(TList)>::parse(columns, value, rowNum);
		}
		iterator(int rowNum, CSVParser &parser) : rowNum(rowNum), parser(parser) {
			if(rowNum >= 0)
				parseTuple();
		}
	public:
		// �����������
		iterator operator++ (int) {
			iterator old = *this;
			if (rowNum >= 0) {
				rowNum++;
				parseTuple();
			}
			return old;
		};
		// ����������
		iterator &operator++ () {
			if (rowNum >= 0) {
				rowNum++;
				parseTuple();
			}
			return *this;
		};
		
		row_type operator* () {
			return value;
		};
		
		row_type* operator-> () {
			return &(value);
		};

		bool operator== (const iterator &second) const {
			if (&parser != &second.parser)
				return false;

			return rowNum == second.rowNum;
		}

		bool operator != (const iterator &second) const {
			return !(*(this) == second);
		}
		friend class CSVParser;
	};

	CSVParser(std::istream &is, size_t skipLines) : is_(is), skipLines_(skipLines) {

	}

	iterator begin() {
		// ��������� ��������� ������
		if (!is_)
			return end();
		// ������ �� ������ ������
		is_.clear();
		is_.seekg(0, std::ios::beg);
		// ���������� ������ ���������� �����
		for (int i = 0; i < skipLines_; i++) {
			std::string line;
			std::getline(is_, line);
		}
		return iterator(0, *this);
	}

	iterator end() {
		// -1 - ������� ���������� ��������
		return iterator(-1, *this);
	}

	char & separator() {
		return separator_;
	}

	char & quote() {
		return quote_;
	}

	char & escape() {
		return escape_;
	}

};