// task4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <fstream>
#include <iostream>
#include <string>
#include <complex>

#include "CSVParser.h"
#include "TuplePrint.h"


int main()
{
	

	std::ifstream file("test.csv");

	try {
		CSVParser<int, std::string, std::complex<float>> parser(file, 0);
		for (auto rs : parser) {
			std::cout << rs << std::endl;
		}
	}
	catch (WrongNumberOfColumns &ex) {
		std::cerr << ex.what();
	}
	catch (WrongDataFormatOfColumn &ex) {
		std::cerr << ex.what();
	}
	
}

