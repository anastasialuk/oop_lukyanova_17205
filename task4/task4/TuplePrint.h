#pragma once

#include <ostream>
#include <tuple>

template<class Tuple, std::size_t N>
struct TuplePrinter {
	static void print(std::ostream &os, const Tuple& t)
	{
		TuplePrinter<Tuple, N - 1>::print(os, t);
		os << ", " << std::get<N - 1>(t);
	}
};

template<class Tuple>
struct TuplePrinter<Tuple, 1> {
	static void print(std::ostream &os, const Tuple& t)
	{
		os << std::get<0>(t);
	}
};

template<class... Args>
std::ostream & operator << (std::ostream &os, const std::tuple<Args...>& t)
{
	os << "(";
	TuplePrinter<decltype(t), sizeof...(Args)>::print(os, t);
	os << ")";
	return os;
}